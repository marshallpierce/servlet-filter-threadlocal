package org.mpierce.http.servlet.headerthreadlocal

import java.util.EnumSet
import javax.servlet.DispatcherType
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.asynchttpclient.Dsl
import org.eclipse.jetty.server.NetworkConnector
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.server.handler.HandlerCollection
import org.eclipse.jetty.servlet.FilterHolder
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.slf4j.MDC

class FilterTest {

    private val client = Dsl.config()
            .build()
            .let { Dsl.asyncHttpClient(it) }
    private val server = Server()
    private var port: Int = 0

    @BeforeEach
    internal fun setUp() {

        val servletHandler = ServletContextHandler()
        servletHandler.contextPath = "/"

        val tl = ThreadLocal<String>()

        listOf(
            ThreadLocalFilter.forHeader("x-foo", tl),
            MdcFilter.forHeader("foo", "x-foo"),
            MdcThreadLocalFilter("foo-tl", tl) { it?.let { "tl-$it" } }
        ).forEach { f ->
            servletHandler.addFilter(FilterHolder(f), "/*", EnumSet.allOf(DispatcherType::class.java))
        }

        servletHandler.addServlet(ServletHolder(object : HttpServlet() {
            override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
                val mdcMap = (MDC.getCopyOfContextMap()?.toMap() ?: mapOf()).toSortedMap()
                resp.status = 200
                resp.writer.write("threadlocal: ${tl.get()}, mdc: $mdcMap")
            }
        }), "/headers")

        val handlerCollection = HandlerCollection()
        handlerCollection.addHandler(servletHandler)

        server.handler = handlerCollection
        server.addConnector(ServerConnector(server))
        server.start()

        port = (server.connectors[0] as NetworkConnector).localPort
    }

    @AfterEach
    internal fun tearDown() {
        client.close()
        server.stop()
    }

    @Test
    internal fun nullWhenNoHeader() {
        val resp = client.prepareGet(("http://localhost:$port/headers")).execute().get()
        assertEquals("threadlocal: null, mdc: {}", resp.responseBody)
    }

    @Test
    internal fun threadLocalSetWhenHasHeader() {
        val resp = client.prepareGet(("http://localhost:$port/headers"))
                .addHeader("x-foo", "asdf")
                .execute()
                .get()
        assertEquals("threadlocal: asdf, mdc: {foo=asdf, foo-tl=tl-asdf}", resp.responseBody)
    }

    @Test
    internal fun threadLocalSetWhenHasHeaderWithEmptyValue() {
        val resp = client.prepareGet(("http://localhost:$port/headers"))
                .addHeader("x-foo", "")
                .execute()
                .get()
        assertEquals("threadlocal: , mdc: {foo=, foo-tl=tl-}", resp.responseBody)
    }
}
