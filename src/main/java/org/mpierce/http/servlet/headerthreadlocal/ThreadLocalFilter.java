package org.mpierce.http.servlet.headerthreadlocal;

import java.io.IOException;
import java.util.function.Function;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * Sets a {@link ThreadLocal} based on incoming HTTP requests. If you just want to use the value of a header as-is in
 * a thread local, see {@link ThreadLocalFilter#forHeader(String, ThreadLocal)}. If you want to perform more complex
 * transformation of the request, provide your own {@link Function}.
 *
 * @param <T> the type contained in the thread local
 */
public class ThreadLocalFilter<T> extends HttpServletFilter {
    private final Function<HttpServletRequest, T> func;
    private final ThreadLocal<T> threadLocal;

    /**
     * @param threadLocal The thread local to set
     * @param func        A function to extract data from the request. The result of evaluating the function on a
     *                    request will be set in the thread local.
     */
    public ThreadLocalFilter(ThreadLocal<T> threadLocal, Function<HttpServletRequest, T> func) {
        this.func = func;
        this.threadLocal = threadLocal;
    }

    /**
     * Construct a filter that sets a {@code ThreadLocal<String>} with the value of an HTTP header, if present.
     *
     * The ThreadLocal will be set to null if the header is not present.
     *
     * @param headerName  The header name to look for
     * @param threadLocal The thread local to populate with the header value
     * @return the configured filter
     */
    public static Filter forHeader(String headerName, ThreadLocal<String> threadLocal) {
        return new ThreadLocalFilter<>(threadLocal, req -> req.getHeader(headerName));
    }

    @Override
    protected void doHttpFilter(HttpServletRequest request, ServletResponse response, FilterChain chain) throws
            IOException, ServletException {
        T result = func.apply(request);

        try {
            threadLocal.set(result);
            chain.doFilter(request, response);
        } finally {
            threadLocal.remove();
        }
    }
}
