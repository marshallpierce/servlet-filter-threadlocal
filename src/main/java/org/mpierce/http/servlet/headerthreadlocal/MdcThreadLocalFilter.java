package org.mpierce.http.servlet.headerthreadlocal;

import java.io.IOException;
import java.util.function.Function;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.slf4j.MDC;

/**
 * Sets keys in SLF4J's MDC based on the contents of a ThreadLocal, as opposed to {@link MdcFilter}, which uses the incoming HTTP request.
 *
 * This is convenient when you're using {@link ThreadLocalFilter} to set a ThreadLocal and you would like to set that value in MDC as well.
 *
 * @see MdcFilter
 * @see ThreadLocalFilter
 */
public class MdcThreadLocalFilter<T> implements Filter {
    private final String mdcKey;
    private final ThreadLocal<T> threadLocal;
    private final Function<T, String> func;

    /**
     * @param mdcKey The key to use in MDC.
     * @param func   A function to calculate MDC contents. The input parameter will be the contents of the threadlocal. If it returns null, no MDC manipulations will be performed.
     */
    public MdcThreadLocalFilter(String mdcKey, ThreadLocal<T> threadLocal, Function<T, String> func) {
        this.mdcKey = mdcKey;
        this.threadLocal = threadLocal;
        this.func = func;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        String result = func.apply(threadLocal.get());
        if (result != null) {
            MDC.put(mdcKey, result);
            try {
                chain.doFilter(request, response);
            } finally {
                MDC.remove(mdcKey);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
        // no op
    }

    @Override
    public void destroy() {
        // no op
    }
}
