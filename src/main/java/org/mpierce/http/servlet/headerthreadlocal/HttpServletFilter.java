package org.mpierce.http.servlet.headerthreadlocal;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

abstract class HttpServletFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
        // no op
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        if (!(request instanceof HttpServletRequest)) {
            // silently proceed if this is used on non-http servlets (does anyone do that?)
            chain.doFilter(request, response);
            return;
        }

        doHttpFilter((HttpServletRequest) request, response, chain);
    }

    @Override
    public void destroy() {
        // no op
    }

    protected abstract void doHttpFilter(HttpServletRequest request, ServletResponse response, FilterChain chain) throws
            IOException, ServletException;
}
