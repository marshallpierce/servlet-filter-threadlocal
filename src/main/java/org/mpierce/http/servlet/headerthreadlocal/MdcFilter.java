package org.mpierce.http.servlet.headerthreadlocal;

import java.io.IOException;
import java.util.function.Function;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.MDC;

/**
 * Sets keys in SLF4J's MDC based on incoming HTTP requests. If you just want to use the value of a header as-is in MDC,
 * see {@link MdcFilter#forHeader(String, String)}. If you want to perform more complex transformation of the request,
 * provide your own {@link Function}.
 *
 * @see MdcThreadLocalFilter
 */
public class MdcFilter extends HttpServletFilter {
    private final String mdcKey;
    private final Function<HttpServletRequest, String> func;

    /**
     * @param mdcKey The key to use in MDC.
     * @param func   A function to calculate MDC contents. If it returns null, no MDC manipulations will be performed.
     */
    public MdcFilter(String mdcKey, Function<HttpServletRequest, String> func) {
        this.mdcKey = mdcKey;
        this.func = func;
    }

    /**
     * Construct a filter that sets the provided MDC key if the specified header is present.
     *
     * @param mdcKey     MDC key to set
     * @param headerName Header name to look for in requests
     * @return the configured filter
     */
    public static Filter forHeader(String mdcKey, String headerName) {
        return new MdcFilter(mdcKey, (req -> req.getHeader(headerName)));
    }

    @Override
    protected void doHttpFilter(HttpServletRequest request, ServletResponse response, FilterChain chain) throws
            IOException, ServletException {
        String result = func.apply(request);
        if (result != null) {
            MDC.put(mdcKey, result);
            try {
                chain.doFilter(request, response);
            } finally {
                MDC.remove(mdcKey);
            }
        } else {
            chain.doFilter(request, response);
        }
    }
}
