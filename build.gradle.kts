import java.util.Date
import com.jfrog.bintray.gradle.BintrayExtension

plugins {
    kotlin("jvm") version "1.3.61"
    id("com.github.ben-manes.versions") version "0.28.0"
    `maven-publish`
    id("com.jfrog.bintray") version "1.8.4"
    id("net.researchgate.release") version "2.8.1"
    id("org.jmailen.kotlinter") version "2.3.1"
}

repositories {
    jcenter()
}

group = "org.mpierce.http.servlet.threadlocal"

val deps by extra {
    mapOf(
        "slf4j" to "1.7.30",
        "junit" to "5.6.0"
    )
}

dependencies {
    api("javax.servlet:javax.servlet-api:3.1.0")
    api("org.slf4j:slf4j-api:${deps["slf4j"]}")

    testImplementation(kotlin("stdlib"))

    testImplementation("org.eclipse.jetty:jetty-servlet:9.4.27.v20200227")
    testImplementation("org.asynchttpclient:async-http-client:2.10.4")
    testRuntimeOnly("ch.qos.logback:logback-classic:1.2.3")

    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}

tasks {
    register<Jar>("sourceJar") {
        from(project.the<SourceSetContainer>()["main"].allSource)
        archiveClassifier.set("sources")
    }

    register<Jar>("docJar") {
        from(project.tasks["javadoc"])
        archiveClassifier.set("javadoc")
    }

    afterReleaseBuild {
        dependsOn(named("bintrayUpload"))
    }
}

configure<PublishingExtension> {
    publications {
        register<MavenPublication>("bintray") {
            from(components["java"])
            artifact(tasks["sourceJar"])
            artifact(tasks["docJar"])
        }
    }
}

configure<BintrayExtension> {
    user = rootProject.findProperty("bintrayUser")?.toString()
    key = rootProject.findProperty("bintrayApiKey")?.toString()
    setPublications("bintray")

    with(pkg) {
        repo = "maven"
        setLicenses("Copyfree")
        vcsUrl = "https://bitbucket.org/marshallpierce/servlet-filter-threadlocal"
        name = "servlet-filter-threadlocal"

        with(version) {
            name = project.version.toString()
            released = Date().toString()
            vcsTag = project.version.toString()
        }
    }
}
