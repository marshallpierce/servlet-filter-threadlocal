[ ![Download](https://api.bintray.com/packages/marshallpierce/maven/servlet-filter-threadlocal/images/download.svg) ](https://bintray.com/marshallpierce/maven/servlet-filter-threadlocal/_latestVersion) 

This library provides some Servlet [`Filter`](https://docs.oracle.com/javaee/7/api/javax/servlet/Filter.html) implementations:

- `ThreadLocalFilter` - set [`ThreadLocal`](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/ThreadLocal.html) data based on the contents of HTTP requests.
- `MdcFilter` - set keys in [MDC](https://www.slf4j.org/manual.html#mdc) based on the contents of HTTP requests. This is commonly used with thread locals (see below) to surface diagnostic info, so it's included here for convenience.
- `MdcThreadLocalFilter` - set MDC keys based on a `ThreadLocal`, as opposed to an HTTP request like `MdcFilter`. If you're using `ThreadLocalFilter` to set a `ThreadLocal`, or otherwise have a `ThreadLocal`, and just want to use the same value in MDC, this filter will do that.

Together with a library like [http-client-threadlocal-header](https://bitbucket.org/marshallpierce/http-client-threadlocal-header/src/master/), you can use this to broadcast per-request information throughout the graph of service calls in a distributed system. 

These filters are not amenable to configuration via `web.xml` since they don't (and can't reasonably) offer a 0-args constructor. Use embedded Jetty or something like that instead so that you can configure your servlets in code rather than XML; you'll be glad you did. See the example below.

# Example

Suppose you want to propagate a request ID header `x-req-id` throughout the system so that you can see logs for a single request across all systems that it touches.

In each service:

- Create a `ThreadLocal` to hold the request id (as a `String`)
- Add a `ThreadLocalFilter` that sets the thread local to be the header's value
- Add a `MdcFilter` that sets an MDC key `req-id` from the header
- Use [http-client-threadlocal-header](https://bitbucket.org/marshallpierce/http-client-threadlocal-header/src/master/) with your HTTP client to set a request header in outbound requests based on the thread local.
- Incorporate MDC into your log format with `%mdc`. See [`PatternLayout`](https://logback.qos.ch/manual/layouts.html#ClassicPatternLayout) for more.

That would look something like this (in Kotlin):

```kotlin
val tl = ThreadLocal<String?>()

// set up an http client (here, Async Http Client)
val client = Dsl.config()
            .addRequestFilter(AsyncHttpClientAdapter(
                    SingleHeaderAdder("x-req-id", tl, 
                            Function.identity())))
            .build()
            .let { Dsl.asyncHttpClient(it) }

// make a Jetty server
val server = Server()

val servletHandler = ServletContextHandler()
servletHandler.contextPath = "/"

// add some filters
listOf(
        // write contents of x-req-id into tl
        ThreadLocalFilter.forHeader("x-req-id", tl),
        // use contents of tl for MDC
        MdcThreadLocalFilter("req-id", tl, Function.identity())
).forEach { f ->
    servletHandler.addFilter(FilterHolder(f), "/*", EnumSet.allOf(DispatcherType::class.java))
}

// add your own app logic
//   - Guice's GuiceFilter
//   - Jersey's ServletContainer
//   - Servlet layers for other libraries
//   - ... or old fashioned hand-rolled servlets

val handlerCollection = HandlerCollection()
handlerCollection.addHandler(servletHandler)

server.handler = handlerCollection
server.addConnector(ServerConnector(server))
server.start()

// your service is running
```
